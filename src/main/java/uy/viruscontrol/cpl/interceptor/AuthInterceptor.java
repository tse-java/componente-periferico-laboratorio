package uy.viruscontrol.cpl.interceptor;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uy.viruscontrol.cpl.persistence.Database;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
@Order(1)
@Slf4j
public class AuthInterceptor extends OncePerRequestFilter {

    private final Database database;

    @Autowired
    public AuthInterceptor(Database database) {
        this.database = database;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (uri.toLowerCase().contains("/api/exams") || uri.toLowerCase().contains("/api/register") && request.getMethod().toLowerCase().equalsIgnoreCase("get") ) {
            String xAuth = request.getHeader("X-Authorization");
            if (xAuth == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Missing Authorization header");
            } else if (!database.isValidToken(xAuth)) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization failed");
            } else {
                filterChain.doFilter(request, response);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
