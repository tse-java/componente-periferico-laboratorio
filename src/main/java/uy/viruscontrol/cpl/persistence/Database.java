package uy.viruscontrol.cpl.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import uy.viruscontrol.cpl.domain.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class Database {
    @Bean
    @Scope("singleton")
    public Database getDatabase() {
        return new Database();
    }

    private final SecureRandom secureRandom = new SecureRandom(); //threadsafe
    private final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe

    //Nuestro Id - examen
    private final HashMap<Integer, Exam> exams = new HashMap<>();
    //Token - Servicio
    private final HashMap<String, ExternalService> externalServices = new HashMap<>();

    private int id = 0;

    public List<Exam> listExams() {
        return new ArrayList<>(exams.values());
    }

    public List<Exam> listExamsByToken(String token) {
        List<Exam> exams = new ArrayList<>(this.exams.values());
        return exams.stream().filter(exam -> exam.getExternalService().getToken().equals(token)).collect(Collectors.toList());
    }

    public Exam findById(Integer examId) {
        return exams.get(examId);
    }

    public Exam addExam(DTOExamRequest dtoExamRequest, String token) {
        ExternalService externalService = externalServices.get(token);
        Exam exam = new Exam();
        exam.setExternalId(dtoExamRequest.getId());
        exam.setStatus(EnumExamStatus.PENDING);
        exam.setId(this.getId());
        exam.setExternalService(externalService);
        exams.put(exam.getId(), exam);
        return exam;
    }


    public int getId() {
        return id++;
    }

    private String generateNewToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }

    public ExternalService addNewService(DTORegisterRequest dtoRegisterRequest) {
        String token = generateNewToken();
        ExternalService externalService = new ExternalService();
        externalService.setToken(token);
        externalService.setDescription(dtoRegisterRequest.getDescription());
        externalServices.put(token, externalService);
        return externalService;
    }

    public boolean isValidToken(String token) {
        return externalServices.get(token) != null;
    }

    public void deleteExams(List<Exam> dtoExamResponseList) {
        dtoExamResponseList.forEach(exam -> exams.remove(exam.getId()));
    }
    public List<ExternalService> listServices() {
        return new ArrayList<>(externalServices.values());
    }
    public void deleteExam(Exam item) {
        this.exams.remove(item.getId());
    }
}
