package uy.viruscontrol.cpl.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uy.viruscontrol.cpl.domain.Exam;
import uy.viruscontrol.cpl.persistence.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ListExams {
    private final Database database;

    @Autowired
    public ListExams(Database database) {
        this.database = database;
    }

    @PostMapping("/delete")
    public String deleteExam(@RequestParam("examId") int examId, Model model) {
        Exam exam = database.findById(examId);
        String tokenParam = exam.getExternalService().getToken();
        database.deleteExam(exam);
        return "redirect:/list?tokenParam="+tokenParam;
    }

    @GetMapping("/list")
    public String greeting(@RequestParam(required = false, defaultValue = "") String tokenParam, Model model) {
        List<String> tokens = database.listServices().stream().map(externalService -> externalService.getToken()).collect(Collectors.toList());
        if(!tokenParam.isEmpty()){
            List<Exam> exams = database.listExamsByToken(tokenParam);
            model.addAttribute("exams",exams);
        }else{
            model.addAttribute("exams",new ArrayList<>());
        }
        model.addAttribute("tokenParam",tokens);
        model.addAttribute("examId","");
        model.addAttribute("tokens",tokens);
        return "list";
    }
}
