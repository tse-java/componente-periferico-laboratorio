package uy.viruscontrol.cpl.controller.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uy.viruscontrol.cpl.domain.*;
import uy.viruscontrol.cpl.persistence.Database;
import uy.viruscontrol.cpl.service.RegisterService;

@RestController
@Slf4j
public class RegisterController implements RegisterService {

    private final Database database;

    @Autowired
    public RegisterController(Database database) {
        this.database = database;
    }

    @Override
    public DTORegisterResponse register(DTORegisterRequest dtoRegisterRequest) {
        ExternalService externalService = database.addNewService(dtoRegisterRequest);
        DTORegisterResponse dtoRegisterResponse = new DTORegisterResponse();
        dtoRegisterResponse.setToken(externalService.getToken());
        return dtoRegisterResponse;
    }

    @Override
    public ResponseEntity<?> checkToken(String token) {
        return ResponseEntity.ok().build();
    }
}
