package uy.viruscontrol.cpl.controller.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import uy.viruscontrol.cpl.domain.EnumExamStatus;
import uy.viruscontrol.cpl.domain.Exam;
import uy.viruscontrol.cpl.persistence.Database;

import java.util.Random;

@Component
@Slf4j
public class ScheduledTasks {

    private final Random random = new Random();

    private final Database database;

    public ScheduledTasks(Database database) {
        this.database = database;
    }

    @Scheduled(fixedRate = 10000)
    public void reportCurrentTime() {
            Exam filteredExam = database.listExams()
                    .stream()
                    .filter(exam -> exam.getStatus().equals(EnumExamStatus.PENDING))
                    .findFirst()
                    .orElse(null);

            if (filteredExam != null) {
                if (Math.random() < 0.6) {
                    log.info("Procesando exam {}", filteredExam);
                    filteredExam.setStatus(EnumExamStatus.DONE);
                    filteredExam.setPositive(random.nextBoolean());
                }else{
                    log.info("Skip tick");
                }
            }
    }
}
