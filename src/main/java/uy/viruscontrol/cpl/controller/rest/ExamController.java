package uy.viruscontrol.cpl.controller.rest;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import uy.viruscontrol.cpl.domain.DTOExamRequest;
import uy.viruscontrol.cpl.domain.DTOExamResponse;
import uy.viruscontrol.cpl.domain.EnumExamStatus;
import uy.viruscontrol.cpl.domain.Exam;
import uy.viruscontrol.cpl.persistence.Database;
import uy.viruscontrol.cpl.service.ExamService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class ExamController implements ExamService {

    private final Database database;

    @Autowired
    public ExamController(Database database) {
        this.database = database;
    }

    @Override
    public void request(DTOExamRequest dtoExamRequest, String token) {
        log.info("New exam: {}", dtoExamRequest);
        Exam exam = database.addExam(dtoExamRequest, token);
    }

    @Override
    public List<DTOExamResponse> fetch(String token) {
        List<Exam> exams = database.listExams();
        List<Exam> dtoExamResponseList = exams
                .stream()
                .filter(exam -> exam.getExternalService().getToken().equals(token) && exam.getStatus().equals(EnumExamStatus.DONE))
                .collect(Collectors.toList());
        database.deleteExams(dtoExamResponseList);
        return dtoExamResponseList.stream().map(exam -> {
            DTOExamResponse dtoExamResponse = new DTOExamResponse();
            dtoExamResponse.setPositive(exam.getPositive());
            dtoExamResponse.setId(exam.getExternalId());
            return dtoExamResponse;
        }).collect(Collectors.toList());
    }

    @GetMapping(value = "/health")
    @Hidden
    public ResponseEntity<?> findByCustomerId() {
        return ResponseEntity.ok().build();
    }
}
