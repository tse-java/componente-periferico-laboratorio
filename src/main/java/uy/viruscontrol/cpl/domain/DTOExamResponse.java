package uy.viruscontrol.cpl.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data

public class DTOExamResponse implements Serializable {
    @Schema(description = "id correspondiente a vuestro sistema")
    private Long id;
    @Schema(description = "resultado del examen")
    private Boolean positive;

    public Boolean getPositive() {
        return positive;
    }

    public void setPositive(Boolean positive) {
        this.positive = positive;
    }
}
