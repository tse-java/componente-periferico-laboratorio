package uy.viruscontrol.cpl.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class DTOExamRequest implements Serializable {
    @Schema(description = "Id que corresponde al sistema externo (componente central). Puede representar una CI, etc.")
    private Long id;
}
