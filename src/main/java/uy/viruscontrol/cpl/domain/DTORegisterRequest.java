package uy.viruscontrol.cpl.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class DTORegisterRequest implements Serializable {
    @Schema(description = "Descripcion del componente central (meramente informativo)"+
                          "ej: grupo16")
    private String description;
}
