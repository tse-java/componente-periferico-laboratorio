package uy.viruscontrol.cpl.domain;

import lombok.Data;

import java.io.Serializable;

@Data

public class Exam implements Serializable {
    private Integer id;
    private Long externalId;
    private Boolean positive;
    private EnumExamStatus status;
    private ExternalService externalService;


    public Boolean getPositive() {
        return positive;
    }

    public void setPositive(Boolean positive) {
        this.positive = positive;
    }
}
