package uy.viruscontrol.cpl.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
public class DTORegisterResponse implements Serializable {
    @Schema(description = "Token asociado al componente central que se esta registrando")
    private String token;
}
