package uy.viruscontrol.cpl.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ConfigURI implements Serializable {
    public String getBaseURI() {
        return baseURI;
    }

    public void setBaseURI(String baseURI) {
        this.baseURI = baseURI;
    }

    @NotNull
    @NotEmpty
    private String baseURI;
}
