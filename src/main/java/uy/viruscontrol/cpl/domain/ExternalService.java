package uy.viruscontrol.cpl.domain;

import lombok.Data;

import java.io.Serializable;

@Data

public class ExternalService implements Serializable {
    private String token;
    private String description;
}
