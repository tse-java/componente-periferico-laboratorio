package uy.viruscontrol.cpl.domain;


public enum EnumExamStatus {
    PENDING(2), // Solicitado al laboratorio, aun no se tiene resultado.
    DONE(3); // Con resultado final.

    private final int code;

    EnumExamStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

