package uy.viruscontrol.cpl.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uy.viruscontrol.cpl.domain.DTOExamRequest;
import uy.viruscontrol.cpl.domain.DTOExamResponse;
import uy.viruscontrol.cpl.domain.DTORegisterRequest;
import uy.viruscontrol.cpl.domain.DTORegisterResponse;

@RequestMapping(value = "/register")
@Tag(name = "Registro")
public interface RegisterService {
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(description = "Crea una nuevo registro de un componente central\n\n" +
            "Emite un token que debe ser utilizado para crear examenes o obtener resultados")
    DTORegisterResponse register(@RequestBody DTORegisterRequest dtoRegisterRequest);

    @GetMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(description = "Verificar token")
    ResponseEntity<?> checkToken(@RequestHeader("X-Authorization") String token);
}

