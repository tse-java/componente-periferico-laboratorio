package uy.viruscontrol.cpl.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import uy.viruscontrol.cpl.domain.DTOExamRequest;
import uy.viruscontrol.cpl.domain.DTOExamResponse;

import java.util.List;

@RequestMapping(value = "/exams")
@Tag(name = "Examenes")
public interface ExamService {
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(description = "Solicita un nuevo examen al sistema. Una vez que el examen sea procesado sera enviado al endpoint especificado.\n\n" +
            "Para mayor simplicidad, el examen solo se intenta entregar una vez.")
    void request(@RequestBody DTOExamRequest dtoExamRequest,
                            @RequestHeader("X-Authorization") String token);

    @GetMapping
    @ResponseStatus(code = HttpStatus.OK)
    @Operation(description = "Retorna los examenes que fueron procesados y faltaban ser reclamados\n")
    List<DTOExamResponse> fetch(@RequestHeader("X-Authorization") String token);
}

